from django.template import engines

import copy

from django_cdstack_deploy.django_cdstack_deploy.func import (
    generate_config_static,
    zip_add_file,
)
from django_cdstack_tpl_centos_7.django_cdstack_tpl_centos_7.views import (
    get_yum_packages,
    get_yum_sources,
)
from django_cdstack_tpl_centos_7.django_cdstack_tpl_centos_7.views import (
    handle as handle_centos_7,
)
from django_cdstack_tpl_dc_tun_clt.django_cdstack_tpl_dc_tun_clt.views import (
    handle as handle_dc_tun_clt,
)
from django_cdstack_tpl_dc_tun_srv.django_cdstack_tpl_dc_tun_srv.views import (
    handle as handle_dc_tun_srv,
)
from django_cdstack_tpl_dyndns_client.django_cdstack_tpl_dyndns_client.views import (
    handle as handle_dyndns_client,
)
from django_cdstack_tpl_crowdsec_main.django_cdstack_tpl_crowdsec_main.views import (
    handle as handle_crowdsec_main,
)
from django_cdstack_tpl_fail2ban.django_cdstack_tpl_fail2ban.views import (
    handle as handle_fail2ban,
)
from django_cdstack_tpl_ipcop.django_cdstack_tpl_ipcop.views import (
    handle as handle_ipcop,
)
from django_cdstack_tpl_uplink_failover.django_cdstack_tpl_uplink_failover.views import (
    handle as handle_uplink_failover,
)
from django_cdstack_tpl_wazuh_agent.django_cdstack_tpl_wazuh_agent.views import (
    handle as handle_wazuh_agent,
)
from django_cdstack_tpl_whclient.django_cdstack_tpl_whclient.views import (
    handle as handle_whclient,
)
from django_cdstack_tpl_shorewall.django_cdstack_tpl_shorewall.views import (
    handle as handle_shorewall,
)


def handle(zipfile_handler, template_opts, cmdb_host, skip_handle_os=False):
    module_prefix = "django_cdstack_tpl_cloudrouter/django_cdstack_tpl_cloudrouter"

    if "yum_packages" not in template_opts:
        template_opts["yum_packages"] = get_yum_packages(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "yum_sources" not in template_opts:
        template_opts["yum_sources"] = get_yum_sources(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    handle_cloudrouter_tunnel_config(
        zipfile_handler, template_opts, cmdb_host, skip_handle_os
    )
    generate_config_static(zipfile_handler, template_opts, module_prefix)

    handle_shorewall(zipfile_handler, template_opts, cmdb_host, skip_handle_os)

    handle_dyndns_client(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_uplink_failover(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_whclient(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_ipcop(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_fail2ban(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_crowdsec_main(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_wazuh_agent(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_centos_7(zipfile_handler, template_opts, cmdb_host, skip_network=True)

    return True


def handle_cloudrouter_tunnel_config(
    zipfile_handler, template_opts, cmdb_host, skip_handle_os
):
    django_engine = engines["django"]

    module_prefix = "django_cdstack_tpl_cloudrouter/django_cdstack_tpl_cloudrouter"

    for key in template_opts.keys():
        if key.startswith("network_tun_conprof_") and key.endswith("_name"):
            key_ident = key[:-5]
            name_ident = key_ident[20:]

            tunnel_conf = dict()

            if template_opts[key_ident + "_type"] == "tinc":
                tunnel_conf["network_tinc_id"] = template_opts[key_ident + "_id"]
                tunnel_conf["network_tinc_rsa_private_key"] = template_opts[
                    key_ident + "_rsa_private_key"
                ]
                tunnel_conf["network_tinc_rsa_public_key"] = template_opts[
                    key_ident + "_rsa_public_key"
                ]

                tunnel_conf["peer_list"] = list()

                for key_peer in template_opts.keys():
                    if key_peer.startswith(key_ident + "_peer_") and key_peer.endswith(
                        "_rsa_public_key"
                    ):
                        key_peer_connection = key_peer[:-15]

                        config_template_file = open(
                            module_prefix
                            + "/templates/config-fs/dynamic/etc/tinc/mode-switch/hosts/peer",
                            "r",
                        ).read()
                        config_template = django_engine.from_string(
                            config_template_file
                        )

                        zip_add_file(
                            zipfile_handler,
                            "etc/tinc/tunnel-"
                            + name_ident
                            + "/hosts/"
                            + template_opts[key_peer_connection + "_id"],
                            config_template.render(
                                {
                                    "network_tinc_rsa_public_key": template_opts[
                                        key_peer_connection + "_rsa_public_key"
                                    ],
                                    "network_tinc_address": template_opts[
                                        key_peer_connection + "_connect_to"
                                    ],
                                }
                            ),
                        )

                        tunnel_conf["peer_list"].append(
                            template_opts[key_peer_connection + "_id"]
                        )

                conf_combined = {**template_opts, **tunnel_conf}

                config_template_file = open(
                    module_prefix
                    + "/templates/config-fs/dynamic/etc/tinc/mode-switch/tinc.conf",
                    "r",
                ).read()
                config_template = django_engine.from_string(config_template_file)

                zip_add_file(
                    zipfile_handler,
                    "etc/tinc/tunnel-" + name_ident + "/tinc.conf",
                    config_template.render(conf_combined),
                )

                config_template_file = open(
                    module_prefix
                    + "/templates/config-fs/dynamic/etc/tinc/mode-switch/rsa_key.priv",
                    "r",
                ).read()
                config_template = django_engine.from_string(config_template_file)

                zip_add_file(
                    zipfile_handler,
                    "etc/tinc/tunnel-" + name_ident + "/rsa_key.priv",
                    config_template.render(conf_combined),
                )

                config_template_file = open(
                    module_prefix
                    + "/templates/config-fs/dynamic/etc/tinc/mode-switch/tinc-up",
                    "r",
                ).read()
                config_template = django_engine.from_string(config_template_file)

                zip_add_file(
                    zipfile_handler,
                    "etc/tinc/tunnel-" + name_ident + "/tinc-up",
                    config_template.render(conf_combined),
                )

                config_template_file = open(
                    module_prefix
                    + "/templates/config-fs/dynamic/etc/tinc/mode-switch/tinc-down",
                    "r",
                ).read()
                config_template = django_engine.from_string(config_template_file)

                zip_add_file(
                    zipfile_handler,
                    "etc/tinc/tunnel-" + name_ident + "/tinc-down",
                    config_template.render(conf_combined),
                )

                config_template_file = open(
                    module_prefix
                    + "/templates/config-fs/dynamic/etc/tinc/mode-switch/hosts/peer-local",
                    "r",
                ).read()
                config_template = django_engine.from_string(config_template_file)

                zip_add_file(
                    zipfile_handler,
                    "etc/tinc/tunnel-"
                    + name_ident
                    + "/hosts/"
                    + template_opts[key_ident + "_id"],
                    config_template.render(conf_combined),
                )

    if "dc_api_url" in template_opts:
        handle_dc_tun_srv(zipfile_handler, template_opts, module_prefix)
        handle_dc_tun_clt(zipfile_handler, template_opts, module_prefix)

    return True
